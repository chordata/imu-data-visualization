import pandas as pd
import json
import pathlib
import argparse
import re

from plot import gyro_acc_mag, show_plots

a_s = 0.000244
m_s = 0.00014
g_s = 0.07

folder = pathlib.Path(__file__).parent
calibrated_masha_csv  = folder.joinpath('calibrated_Tecnocampus_masha_06.csv')
masha_csv  = folder.joinpath('Tecnocampus_masha_06.csv')

USE_CSV = masha_csv
NODE_NAME = "l-foot"

df = pd.read_csv(USE_CSV)

node_df = df[df['node_label'] == NODE_NAME] 
print(node_df)

label = "(world) RAW IMU: " if USE_CSV == calibrated_masha_csv else " RAW IMU: "
label += NODE_NAME

gyro_acc_mag(NODE_NAME, node_df, label, a_s, m_s, g_s)

show_plots()