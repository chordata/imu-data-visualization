# IMU visualization

Utility script to visualize IMU data recorded in CSV

## Install with venv

```bash
python -m venv venv  
venv/bin/pip install matplotlib pandas numpy
```

## Run

```bash
venv/bin/python plot_IMU_data.py
```

## Options

Changing the `USE_CSV` to `masha_csv` or `calibrated_masha_csv` will print the data in either sensor or world reference frame.

Set `NODE_NAME` to either of the sensor labels included in the CSV

