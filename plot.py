import numpy as np
import pandas as pd
import matplotlib.pyplot as pl
#
# fuction to plot df variables
# input data in the node_name and the data frame from where
# de acc, gyro and mag variables are read
#

def gyro_acc_mag(node_name, df, label, a_s, m_s, g_s):
       
    [n_samples, n_col] = df.shape
    samples = np.arange(n_samples)
    pl.figure(node_name + ' ' + label)
    pl.suptitle(node_name + ' ' + label, size=18)
    # acc
    ax1 = pl.subplot(3,3,1)
    pl.plot(samples, df.a_x * a_s, 'r')
    pl.ylabel('Acceleration \n [g]', multialignment='center')
    pl.gca().set_title('X')
    pl.grid()
    pl.subplot(3,3,2, sharex=ax1)
    pl.plot(samples, df.a_y * a_s, 'g')
    pl.gca().set_title('Y')
    pl.grid()
    pl.subplot(3,3,3, sharex=ax1)
    pl.plot(samples, df.a_z * a_s, 'b')
    pl.gca().set_title('Z')
    pl.grid()
    # gyro
    pl.subplot(3,3,4, sharex=ax1)
    pl.plot(samples, df.g_x * g_s, 'r')
    pl.ylabel('Angular vel \n [deg/seg]', multialignment='center')
    pl.grid()
    pl.subplot(3,3,5, sharex=ax1)
    pl.plot(samples, df.g_y * g_s, 'g')
    pl.grid()
    pl.subplot(3,3,6, sharex=ax1)
    pl.plot(samples, df.g_z * g_s, 'b')
    pl.grid()
    # mag
    pl.subplot(3,3,7, sharex=ax1)
    pl.plot(samples, df.m_x * m_s, 'r')
    pl.ylabel('Mag field \n [Gauss]', multialignment='center')
    pl.grid()
    pl.subplot(3,3,8, sharex=ax1)
    pl.plot(samples, df.m_y * m_s, 'g')
    pl.grid()
    pl.subplot(3,3,9, sharex=ax1)
    pl.plot(samples, df.m_z * m_s, 'b')
    pl.grid()

def show_plots():
    pl.show()